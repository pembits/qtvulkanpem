#include <QApplication>
#include <QLoggingCategory>
#include "mainwindow.h"
#include "vulkanwindow.h"

#include "ui_mainwindowpem.h"

int main(int argc, char **argv)
{
	QApplication app(argc, argv);

	const bool dbg = qEnvironmentVariableIntValue("QT_VK_DEBUG");

	QVulkanInstance inst;

	if (dbg) {
		QLoggingCategory::setFilterRules(QStringLiteral("qt.vulkan=true"));

#ifndef Q_OS_ANDROID
		inst.setLayers(QByteArrayList() << "VK_LAYER_LUNARG_standard_validation");
#else
		inst.setLayers(QByteArrayList()
					   << "VK_LAYER_GOOGLE_threading"
					   << "VK_LAYER_LUNARG_parameter_validation"
					   << "VK_LAYER_LUNARG_object_tracker"
					   << "VK_LAYER_LUNARG_core_validation"
					   << "VK_LAYER_LUNARG_image"
					   << "VK_LAYER_LUNARG_swapchain"
					   << "VK_LAYER_GOOGLE_unique_objects");
#endif
	}

	if (!inst.create())
		qFatal("Failed to create Vulkan instance: %d", inst.errorCode());

	VulkanWindow *vulkanWindow = new VulkanWindow(dbg);
	vulkanWindow->setVulkanInstance(&inst);

	//MainWindow mainWindow(vulkanWindow);
	//mainWindow.resize(1024, 768);
	//mainWindow.show();

	MainWindowPem mainWindowPem;
	//static_cast<MainWindow*>(mainWindowPem.centralWidget())->Init(vulkanWindow);
	mainWindowPem.ui->widgetMain->Init(vulkanWindow);
	mainWindowPem.resize(1024, 768);
	mainWindowPem.show();

	return app.exec();
}
