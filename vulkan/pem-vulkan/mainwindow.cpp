#include "mainwindow.h"
#include "vulkanwindow.h"
#include <QApplication>
#include <QLabel>
#include <QPushButton>
#include <QLCDNumber>
#include <QCheckBox>
#include <QGridLayout>

void MainWindow::Init(VulkanWindow *vulkanWindow)
{
	QWidget *wrapper = QWidget::createWindowContainer(vulkanWindow);
	wrapper->setFocusPolicy(Qt::StrongFocus);
	wrapper->setFocus();

	meshSwitch = new QCheckBox(tr("&Use Qt logo"));
	meshSwitch->setFocusPolicy(Qt::NoFocus); // do not interfere with vulkanWindow's keyboard input

	counterLcd = new QLCDNumber(5);
	counterLcd->setSegmentStyle(QLCDNumber::Filled);
	counterLcd->display(m_count);

	newButton = new QPushButton(tr("&Add new"));
	newButton->setFocusPolicy(Qt::NoFocus);
	quitButton = new QPushButton(tr("&Quit"));
	quitButton->setFocusPolicy(Qt::NoFocus);
	pauseButton = new QPushButton(tr("&Pause"));
	pauseButton->setFocusPolicy(Qt::NoFocus);

	connect(quitButton, &QPushButton::clicked, qApp, &QCoreApplication::quit);
	connect(newButton, &QPushButton::clicked, vulkanWindow, [=] {
		vulkanWindow->addNew();
		m_count = vulkanWindow->instanceCount();
		counterLcd->display(m_count);
	});
	connect(pauseButton, &QPushButton::clicked, vulkanWindow, &VulkanWindow::togglePaused);
	connect(meshSwitch, &QCheckBox::clicked, vulkanWindow, &VulkanWindow::meshSwitched);

	QGridLayout *layout = new QGridLayout;
	layout->addWidget(meshSwitch, 1, 2);
	layout->addWidget(createLabel(tr("INSTANCES")), 2, 2);
	layout->addWidget(counterLcd, 3, 2);
	layout->addWidget(newButton, 4, 2);
	layout->addWidget(pauseButton, 5, 2);
	layout->addWidget(quitButton, 6, 2);
	layout->addWidget(wrapper, 0, 0, 7, 2);
	setLayout(layout);
}

MainWindow::MainWindow(VulkanWindow *vulkanWindow)// : MainWindowPem()
{
	Init(vulkanWindow);
}

QLabel *MainWindow::createLabel(const QString &text)
{
	QLabel *lbl = new QLabel(text);
	lbl->setAlignment(Qt::AlignHCenter | Qt::AlignBottom);
	return lbl;
}
