#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>

#include "mainwindowpem.h"

QT_BEGIN_NAMESPACE
class QLCDNumber;
class QLabel;
class QPushButton;
class QCheckBox;
QT_END_NAMESPACE

class VulkanWindow;

class MainWindow : public QWidget
{
public:
	MainWindow(QWidget *widget) {}
	MainWindow(VulkanWindow *vulkanWindow);
	void Init(VulkanWindow *vulkanWindow);

private:
	QLabel *createLabel(const QString &text);

	QCheckBox *meshSwitch;
	QLCDNumber *counterLcd;
	QPushButton *newButton;
	QPushButton *quitButton;
	QPushButton *pauseButton;

	int m_count = 128;
};

#endif
