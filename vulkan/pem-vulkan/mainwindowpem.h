#ifndef MAINWINDOWPEM_H
#define MAINWINDOWPEM_H

#include <QMainWindow>

namespace Ui {
class MainWindowPem;
}

class MainWindowPem : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindowPem(QWidget *parent = nullptr);
	~MainWindowPem();

public:
	Ui::MainWindowPem *ui;
};

#endif // MAINWINDOWPEM_H
