QT += widgets concurrent

HEADERS += \
	mainwindow.h \
	mainwindowpem.h \
	vulkanwindow.h \
	renderer.h \
	mesh.h \
	shader.h \
	camera.h

SOURCES += \
	main.cpp \
	mainwindow.cpp \
	mainwindowpem.cpp \
	vulkanwindow.cpp \
	renderer.cpp \
	mesh.cpp \
	shader.cpp \
	camera.cpp

RESOURCES += hellovulkancubes.qrc

# install
target.path = $$[QT_INSTALL_EXAMPLES]/vulkan/pem-vulkan
INSTALLS += target

FORMS += \
	mainwindowpem.ui
