/********************************************************************************
** Form generated from reading UI file 'mainwindowpem.ui'
**
** Created by: Qt User Interface Compiler version 5.13.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOWPEM_H
#define UI_MAINWINDOWPEM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>
#include "mainwindow.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindowPem
{
public:
    QAction *actiond;
    QAction *actiond_2;
    QAction *actione;
    QAction *actionf;
    QWidget *centralwidget;
    QHBoxLayout *horizontalLayout;
    MainWindow *widgetMain;
    QPushButton *pushButton;
    QMenuBar *menubar;
    QMenu *menua;
    QMenu *menuc;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindowPem)
    {
        if (MainWindowPem->objectName().isEmpty())
            MainWindowPem->setObjectName(QString::fromUtf8("MainWindowPem"));
        MainWindowPem->resize(801, 600);
        actiond = new QAction(MainWindowPem);
        actiond->setObjectName(QString::fromUtf8("actiond"));
        actiond_2 = new QAction(MainWindowPem);
        actiond_2->setObjectName(QString::fromUtf8("actiond_2"));
        actione = new QAction(MainWindowPem);
        actione->setObjectName(QString::fromUtf8("actione"));
        actionf = new QAction(MainWindowPem);
        actionf->setObjectName(QString::fromUtf8("actionf"));
        centralwidget = new QWidget(MainWindowPem);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        horizontalLayout = new QHBoxLayout(centralwidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        widgetMain = new MainWindow(centralwidget);
        widgetMain->setObjectName(QString::fromUtf8("widgetMain"));

        horizontalLayout->addWidget(widgetMain);

        pushButton = new QPushButton(centralwidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(pushButton->sizePolicy().hasHeightForWidth());
        pushButton->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(pushButton);

        MainWindowPem->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindowPem);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 801, 26));
        menua = new QMenu(menubar);
        menua->setObjectName(QString::fromUtf8("menua"));
        menuc = new QMenu(menubar);
        menuc->setObjectName(QString::fromUtf8("menuc"));
        MainWindowPem->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindowPem);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindowPem->setStatusBar(statusbar);

        menubar->addAction(menua->menuAction());
        menubar->addAction(menuc->menuAction());
        menua->addAction(actionf);
        menuc->addAction(actiond);

        retranslateUi(MainWindowPem);

        QMetaObject::connectSlotsByName(MainWindowPem);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindowPem)
    {
        MainWindowPem->setWindowTitle(QCoreApplication::translate("MainWindowPem", "MainWindow", nullptr));
        actiond->setText(QCoreApplication::translate("MainWindowPem", "d", nullptr));
        actiond_2->setText(QCoreApplication::translate("MainWindowPem", "d", nullptr));
        actione->setText(QCoreApplication::translate("MainWindowPem", "e", nullptr));
        actionf->setText(QCoreApplication::translate("MainWindowPem", "E&xit", nullptr));
        pushButton->setText(QCoreApplication::translate("MainWindowPem", "PushButton", nullptr));
        menua->setTitle(QCoreApplication::translate("MainWindowPem", "&File", nullptr));
        menuc->setTitle(QCoreApplication::translate("MainWindowPem", "&View", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindowPem: public Ui_MainWindowPem {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOWPEM_H
